package com.example.preexamenj;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;



public class ReciboNominaActivity extends AppCompatActivity {
    private TextView lblNumRecibo;
    private TextView lblNombre;
    private EditText txtHorasNormal;
    private EditText txtHorasExtra;
    private RadioGroup rgPuesto;
    private RadioButton rdbPuesto1;
    private RadioButton rdbPuesto2;
    private RadioButton rdbPuesto3;
    private TextView lblSubtotal;
    private TextView lblImpuesto;
    private TextView lblTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private ReciboNomina reciboNomina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void iniciarComponentes() {
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        lblNombre = findViewById(R.id.lblNombre);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtra = findViewById(R.id.txtHorasExtra);
        rdbPuesto1 = findViewById(R.id.rdbPuesto1);
        rdbPuesto2 = findViewById(R.id.rdbPuesto2);
        rdbPuesto3 = findViewById(R.id.rdbPuesto3);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        lblTotal = findViewById(R.id.lblTotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        rgPuesto = findViewById(R.id.rgPuesto);

        Random random = new Random();
        int numeroRecibo = random.nextInt(10000);
        String strNombre = getIntent().getStringExtra("strNombre");

        lblNombre.setText("Nombre: " + strNombre);
        lblNumRecibo.setText("Numero de Recibo: " + String.valueOf(numeroRecibo));

        reciboNomina = new ReciboNomina(numeroRecibo, strNombre, 0.0, 0.0, 1, 0.16);
    }

    private void calcular() {
        int puesto = rgPuesto.getCheckedRadioButtonId();
        if (txtHorasExtra.getText().toString().isEmpty() || txtHorasNormal.getText().toString().isEmpty() || puesto == -1) {
            Toast.makeText(getApplicationContext(), "Datos Requeridos", Toast.LENGTH_SHORT).show();
            return;
        }

        if (puesto == rdbPuesto1.getId()) {
            reciboNomina.setPuesto(1);
        } else if (puesto == rdbPuesto2.getId()) {
            reciboNomina.setPuesto(2);
        } else if (puesto == rdbPuesto3.getId()) {
            reciboNomina.setPuesto(3);
        }

        reciboNomina.setHorasTrabExtras(Double.parseDouble(txtHorasExtra.getText().toString()));
        reciboNomina.setHorasTrabNormal(Double.parseDouble(txtHorasNormal.getText().toString()));

        String subtotal = String.format("%.3f",reciboNomina.calcularSubtotal());
        String impuesto = String.format("%.3f",reciboNomina.calcularImpuesto());
        String total = String.format("%.3f", reciboNomina.calcularTotal());
        lblSubtotal.setText("subtotal: " + subtotal + "MXN");
        lblImpuesto.setText("impuesto: "+ impuesto + "MXN");
        lblTotal.setText("total: " + total + "MXN");
    }

    private void limpiar() {
        rgPuesto.clearCheck();
        txtHorasExtra.getText().clear();
        txtHorasNormal.getText().clear();
        lblSubtotal.setText("Subtotal");
        lblImpuesto.setText("Impuesto");
        lblTotal.setText("Total a Pagar");
        txtHorasNormal.requestFocus();
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Nomina");
        confirmar.setMessage("¿Desea Regresar?");
        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialog, which) -> {}).show();
    }

}
