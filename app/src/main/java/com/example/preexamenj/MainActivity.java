package com.example.preexamenj;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;;import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnSalir;
    private Button btnEntrar;
    private EditText txtNombre;
    private TextView lblNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
        txtNombre = findViewById(R.id.txtNombre);
    }

    private void entrar() {
        if (txtNombre.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Nombre requerido", Toast.LENGTH_SHORT).show();
            return;
        }
        String strNombre = txtNombre.getText().toString();
        Intent intent = new Intent(this, ReciboNominaActivity.class);
        intent.putExtra("strNombre", strNombre);
        startActivity(intent);
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Nomina");
        confirmar.setMessage("¿Deseas salir?");
        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialog, which) -> {
        }).show();
    }
}